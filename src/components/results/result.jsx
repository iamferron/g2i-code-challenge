import * as React from 'react';
import decodeEntities from '../../utils/decode-entities';

import './result.scss';

export const Result = ({ question, isCorrect }) => (
  <div className={`result ${isCorrect ? 'correct' : 'incorrect'}`}>
    <div className="icon">{isCorrect ? '+' : '-'}</div>
    <div className="question">{decodeEntities(question)}</div>
  </div>
);
