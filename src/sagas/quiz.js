import { call, put } from 'redux-saga/effects';
import fetch from 'isomorphic-fetch';

export const API_URL = 'https://opentdb.com/api.php?amount=10&difficulty=hard&type=boolean';

export const FETCH_QUIZ = 'g2i-code-challenge/quiz/FETCH_QUIZ';
export const FETCH_QUIZ_REQUEST = 'g2i-code-challenge/quiz/FETCH_QUIZ_REQUEST';
export const FETCH_QUIZ_SUCCEEDED = 'g2i-code-challenge/quiz/FETCH_QUIZ_SUCCEEDED';
export const FETCH_QUIZ_FAILED = 'g2i-code-challenge/quiz/FETCH_QUIZ_FAILED';

export const ANSWER_QUESTION = 'g2i-code-challenge/quiz/ANSWER_QUESTION';
export const RESET_ANSWERS = 'g2i-code-challenge/quiz/RESET_ANSWERS';

const initialState = {
  quizIsLoading: false,
  quizError: null,
  quizQuestions: null,
  quizAnswers: null,
};

let answers;
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case FETCH_QUIZ_REQUEST:
      return { ...initialState, quizIsLoading: true };

    case FETCH_QUIZ_SUCCEEDED:
      return {
        ...state,
        quizError: null,
        quizQuestions: action.questions,
        quizAnswers: new Array(action.questions.length),
        quizIsLoading: false,
      };

    case FETCH_QUIZ_FAILED:
      return {
        ...state,
        quizError: action.error,
        quizQuestions: null,
        quizAnswers: null,
        quizIsLoading: false,
      };

    case ANSWER_QUESTION:
      answers = state.quizAnswers;
      answers[action.questionNumber - 1] = action.answer;
      return { ...state, quizAnswers: answers };

    case RESET_ANSWERS:
      return { ...state, quizAnswers: new Array(state.quizQuestions.length) };

    default:
      return state;
  }
}

export async function fetchQuizQuestionsFromAPI() {
  const response = await fetch(API_URL);
  const data = await response.json();
  return data.results;
}

export function answerQuestion(questionNumber, answer) {
  return { type: ANSWER_QUESTION, questionNumber, answer };
}

export function resetAnswers() {
  return { type: RESET_ANSWERS };
}

export function fetchQuiz() {
  return { type: FETCH_QUIZ };
}

export function* fetchQuizSaga() {
  yield put({ type: FETCH_QUIZ_REQUEST });
  try {
    const questions = yield call(fetchQuizQuestionsFromAPI);
    yield put({ type: FETCH_QUIZ_SUCCEEDED, questions });
  } catch (error) {
    yield put({ type: FETCH_QUIZ_FAILED, error });
  }
}
