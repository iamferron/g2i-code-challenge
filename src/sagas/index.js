import { takeLatest } from 'redux-saga/effects';
import { FETCH_QUIZ, fetchQuizSaga } from './quiz';

export default function* rootSaga() {
  yield takeLatest(FETCH_QUIZ, fetchQuizSaga);
}
